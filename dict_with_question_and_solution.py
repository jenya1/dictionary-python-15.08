
import random
def add_random_number_to_list(l2):
    l2.append(random.randint(1, 100))
    #return l2

def change_x(x):
    x = random.randint(1, 100)

l1 = [1,2,3]

add_random_number_to_list(l1)

print(l1)

x = 1
change_x(x)
print(x)
print('================================================')

#set is anunsorted unique list that have no duplicates
a = set()
print(type(a)) # class 'set'
a = {}
print(type(a)) #class 'dict'
a = {5}
print(type(a)) #class 'set'
a = set()
a.add(1)
print(a) #{1}
a.add(1)
print(a) #{1} - still the same because no duplicates in set
list1 = [1, 1 , 4 , 5 , 6 , 7, 1, 4, -3]
print(set(list1)) # {1, 4, 5, 6, 7, -3} it became a list without duplicates
import random

l1 = [random.randint(1, 100) for n in range(100)]

l1 = sorted(l1)

s1 = set(l1)
print(s1)
print(len(s1))

#create dictionary with duplicated values
# 1 : 'o' one
# 2 : 't' two
# 3 : 't'
# 4 : 'f'
# 5 : 'f'
# ... 10

# write a loop to display how many times
# each letter repeats itself
print('====================================================================================')
lettersDict = { 1: 'o', 2: 't', 3: 't', 4: 'f', 5: 'f', 6: 's', 7: 's', 8: 'e', 9: 'n', 10: 't'}
print(lettersDict)
print(f'values without set = {list(lettersDict.values())}')
set_keys_dict = set(lettersDict.values())
print(set_keys_dict)

dic_letters_count = dict()
for n in set_keys_dict:
    dic_letters_count[n] = 0
print(dic_letters_count)
# {'s': 0, 'o': 0, 'e': 0, 'f': 0, 'n': 0, 't': 0}

for v in list(lettersDict.values()):
    count = dic_letters_count[v]
    dic_letters_count[v] = count + 1

print(dic_letters_count)

